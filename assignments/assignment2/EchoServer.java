/*
This is the solution for Lab 3 - SecAD, Spring 2018.
I put this sample for someone who did not complete Lab 3
to work on assignment 2.
*/
import java.net.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;

public class EchoServer {
	static ThreadList threadlist = new ThreadList();
    public static void main(String[] args) throws IOException {
        
        if (args.length != 1) {
            System.err.println("Usage: java EchoServer <port number>");
            System.exit(1);
        }
        
        int portNumber = Integer.parseInt(args[0]);
        
        try {
            ServerSocket serverSocket =
                new ServerSocket(Integer.parseInt(args[0]));
            System.out.println("EchoServer is running at port " + Integer.parseInt(args[0]));
			while(true){		    
			    Socket clientSocket = serverSocket.accept(); 
		        System.out.println("A client is connected "); 
				EchoServerThread newthread = new EchoServerThread(threadlist,clientSocket);	
				threadlist.addThread(newthread);
				newthread.start();  
			} 
         } catch (IOException e) {
            System.out.println("Exception caught when trying to listen on port "
                + portNumber + " or listening for a connection");
            System.out.println(e.getMessage());
        }
    }
}

class EchoServerThread extends Thread{
	private Socket clientSocket = null;
	private ThreadList threadlist = null;
	private PrintWriter out = null;
	private BufferedReader in = null;
	public EchoServerThread(Socket socket){
		clientSocket = socket;
	}
	public EchoServerThread(ThreadList threadlist,Socket socket){
		this.threadlist = threadlist;
		clientSocket = socket;
	}
	public void send(String message){
		if (out!=null) out.println(message);
	}
	public void run(){
		System.out.println("A new thread for client is running");

			
		try{		
			/*PrintWriter*/ out =
		            new PrintWriter(clientSocket.getOutputStream(), true);                   
		    /*BufferedReader*/ in = new BufferedReader(
		            new InputStreamReader(clientSocket.getInputStream()));
			if(threadlist!=null){
				System.out.println("Inside thread: total clients: " +threadlist.getNumberofThreads());
				threadlist.sendToAll("To All: the number of connected client:" + threadlist.getNumberofThreads());		
			}		
		    String inputLine;
		    while ((inputLine = in.readLine()) != null) {
		            System.out.println("received from client: " + inputLine);
		            System.out.println("Echo back");
		            //out.println(inputLine);
					if(inputLine.equals("<exit>")){
						threadlist.sendToAll("To All: A client exists, the number of connected client:" + (threadlist.getNumberofThreads()-1));
						threadlist.removeThread(this);
						clientSocket.close();
					}else{
						threadlist.sendToAll("To All <new message>:"+ inputLine);
					}
		    }
		}catch(IOException ioe){
			System.out.println("Exception in thread: " + ioe.getMessage());
		}
	}
}

class ThreadList{
	private ArrayList<EchoServerThread> threadlist = new ArrayList<EchoServerThread>();	
	public ThreadList(){
			
	}
	public synchronized int getNumberofThreads(){
		return threadlist.size();
	}
	public synchronized void addThread(EchoServerThread newthread){
		threadlist.add(newthread);
		
	}
	public synchronized void removeThread(EchoServerThread thread){
		threadlist.remove(thread);
		
	}
	public void sendToAll(String message){
		Iterator<EchoServerThread> threadlistIterator = threadlist.iterator();
		while(threadlistIterator.hasNext()){
			EchoServerThread thread = threadlistIterator.next();
			thread.send(message); 
		}
	}
}

